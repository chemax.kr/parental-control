package timer

import (
	"fmt"
	"time"

	"github.com/robfig/cron"
)

func StartCron(timerUpdate func()) {
	c := cron.New()
	c.AddFunc("@every 2s", func() {
		fmt.Println(time.Now())
		timerUpdate()
	})
	c.Start()
}
