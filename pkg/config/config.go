package config

import (
	"encoding/json"
	"fmt"
	"os"
	"time"
)

const layoutISO = "2006-01-02"

type Timers struct {
	MaxPlayTime         int `json:"maxPlayTime,omitempty"`
	CurrPlayTime        int
	CurrPlayFlag        bool
	PlayBeforeBreakTime int `json:"playBeforeBreakTime,omitempty"`
	BreakTime           int `json:"breakTime,omitempty"`
	CurrBreakTime       int
	CurrBreakFlag       bool
	CurrBeforeBreakTime int
}

type Config struct {
	Timers *Timers `json:"timers,omitempty"`
}

func New() *Config {
	cfg := &Config{}
	cfg.Read()

	// t := &models.Timers{
	// 	MaxPlayTime:         10,
	// 	PlayBeforeBreakTime: 4,
	// 	CurrBeforeBreakTime: 4,
	// 	BreakTime:           3,
	// 	CurrPlayFlag:        true,
	// }
	return cfg
}

func (c *Config) Read() {
	data, err := os.ReadFile("config.json")
	if err != nil {
		fmt.Println(err.Error())
		os.Exit(1)
	}
	if err := json.Unmarshal(data, c); err != nil {
		fmt.Println(err.Error())
		os.Exit(1)
	}
	c.Timers.CurrBeforeBreakTime = c.Timers.PlayBeforeBreakTime
	c.Timers.CurrPlayFlag = true
}

func (c *Config) Write() {
	data, err := json.Marshal(c)
	if err != nil {
		fmt.Println(err.Error())
		return
	}
	os.WriteFile(cfgFileName(), data, 0740)
}

func cfgFileName() string {
	t := time.Now()
	return fmt.Sprintf("days/%s.json", t.Format(layoutISO))
}
