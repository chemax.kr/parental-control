package app

import (
	"context"
	"fmt"

	"gitlab.com/chemax.kr/parental-control/pkg/config"
	"gitlab.com/chemax.kr/parental-control/pkg/timer"
)

func Run(ctx context.Context, cfg *config.Config) error {

	t := cfg.Timers

	timer.StartCron(func() {
		defer func() {
			cfg.Write()
		}()
		if t.CurrBreakFlag {
			t.CurrBreakTime--
			if t.CurrBreakTime < 1 {
				t.CurrBreakFlag = false
				t.CurrPlayFlag = true
				t.CurrBeforeBreakTime = t.PlayBeforeBreakTime
				fmt.Println("Конец перерыва")
			}
		}

		if t.CurrPlayFlag {
			t.CurrPlayTime++
			t.CurrBeforeBreakTime--
			t.MaxPlayTime--
			if t.MaxPlayTime < 1 {
				fmt.Printf("Time end\r\n")
				t.CurrPlayFlag = false
				t.CurrBreakFlag = false
				return
			}
			if t.CurrBeforeBreakTime < 1 {
				t.CurrPlayFlag = false
				t.CurrBreakFlag = true
				t.CurrBreakTime = t.BreakTime
				fmt.Printf("перерыв на %d\r\n", t.BreakTime)
				return
			}

		}

	})
	return nil
}
