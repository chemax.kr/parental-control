package main

import (
	"context"
	"fmt"
	"io/fs"
	"os"

	"gitlab.com/chemax.kr/parental-control/pkg/app"
	"gitlab.com/chemax.kr/parental-control/pkg/config"
)

var version = "local_build"

func main() {
	if len(os.Args) == 2 && os.Args[1] == "version" {
		fmt.Println(version)
		os.Exit(0)
	}
	var fm fs.FileMode = 0740
	os.Mkdir("days", fm)
	cfg := config.New()
	if err := app.Run(context.Background(), cfg); err != nil {
		fmt.Println(err.Error())
	}
	fmt.Scanf(" ")
}
